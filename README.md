# README #

Very simple app for testing video chat using opentok

What you could do

* visit the home page (http://polar-atoll-8962.herokuapp.com/)
* create a session
* open another browser / ask someone to visit the home page from another machine
* ask him/her to select the session you created
* Now u should be able to chat via video

create an account with [https://tokbox.com/](https://tokbox.com/)

Please change API_KEY and SECRET to yours in 
```
#!ruby

config/application.rb
```
 file