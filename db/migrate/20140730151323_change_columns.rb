class ChangeColumns < ActiveRecord::Migration
  def change
    change_column :sessions, :sid, :text
    change_column :sessions, :token, :text
  end
end
