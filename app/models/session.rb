require 'opentok'

class Session < ActiveRecord::Base
  before_create :opentok_session

  def opentok_session
    opentok = OpenTok::OpenTok.new API_KEY, SECRET
    session = opentok.create_session
    self.sid = session.session_id
    self.token = opentok.generate_token(session.session_id)
  end
end
